import oscP5.*;
import netP5.*;

OscP5 oscP5;
NetAddress myRemoteLocation;

String entree = "";
int longueur;

String f_entree ;


void setup() {
  size(400,400);
  frameRate(12);
  
  
  
  /* start oscP5, listening for incoming messages at port 12000 */
  oscP5 = new OscP5(this,32000);
  
  /* myRemoteLocation is a NetAddress. a NetAddress takes 2 parameters,
   * an ip address and a port number. myRemoteLocation is used as parameter in
   * oscP5.send() when sending osc packets to another computer, device, 
   * application. usage see below. for testing purposes the listening port
   * and the port of the remote location address are the same, hence you will
   * send messages back to this sketch.
   */
  myRemoteLocation = new NetAddress("10.0.1.74",32000);

}

void draw() {
  background(0);
  cible();
  bonjour();
  chat();
}

void bonjour () {
  /* in the following different ways of creating osc messages are shown by example */
  OscMessage myMessage = new OscMessage("/test");
  myMessage.add(entree);
  oscP5.send(myMessage, myRemoteLocation); 
}


void chat(){
  fill(75);
  rect(5,5,100,20);
  fill(255);
  text(entree,10,10);
  if (keyPressed == true){
    entree = entree + key;
    
    if (key == BACKSPACE){
      longueur = entree.length();
      f_entree = entree.substring(0,longueur-2);
      print(f_entree);
      entree = f_entree;
    }
  }
}

void cible(){
  stroke(#FFFFFF);
  strokeWeight(1);
  line(mouseX-5, mouseY-5, mouseX-5, mouseY+5);
}
