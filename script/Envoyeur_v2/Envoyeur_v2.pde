import oscP5.*;
import netP5.*;


// import de la librairie ControlP5
import controlP5.*;

ControlP5 cp5;


OscP5 oscP5;
NetAddress myRemoteLocation;

//changemement du nom de la variable string
String textValue = "";


// ===================================================


void setup() {
  // taille verticale du chat
  size(400, 600);
  background(0);

  frameRate(12);

  PFont font = createFont("arial", 20);

cp5 = new ControlP5(this);
/*
cp5.addTextfield("textValue")
     .setPosition(20,170)
     .setSize(200,40)
     .setFont(createFont("arial",20))
     .setAutoClear(false)
     ;*/
  
  cp5.addTextfield("input")
    .setPosition(100, 500)
    .setSize(200, 40)
    .setFont(font)
    .setFocus(true)
    .setColor(color(255, 0, 0))
    ;



  /* start oscP5, listening for incoming messages at port 12000 */
  oscP5 = new OscP5(this, 32000);

  /* myRemoteLocation is a NetAddress. a NetAddress takes 2 parameters,
   * an ip address and a port number. myRemoteLocation is used as parameter in
   * oscP5.send() when sending osc packets to another computer, device, 
   * application. usage see below. for testing purposes the listening port
   * and the port of the remote location address are the same, hence you will
   * send messages back to this sketch.
   */
  myRemoteLocation = new NetAddress("127.0.0.1", 32000);



  textFont(font);
}


// ===================================================



/*espace de saissie que doit recevoir le texte de chacun*/
void draw() {
  background(0);
  bonjour();


  text(cp5.get(Textfield.class, "input").getText(), 100, 130);
  text(textValue, 100, 180);
}



// ===================================================



void controlEvent(ControlEvent theEvent) {
  if (theEvent.isAssignableFrom(Textfield.class)) {
    println("controlEvent: accessing a string from controller '"
      +theEvent.getName()+"': "
      +theEvent.getStringValue()
      );
  }
}


public void input(String theText) {
  // automatically receives results from controller input
  println("a textfield event for controller 'input' : "+theText);
}



void bonjour () {
  /* in the following different ways of creating osc messages are shown by example */
  OscMessage myMessage = new OscMessage("/test");
  myMessage.add(textValue);
  oscP5.send(myMessage, myRemoteLocation);
}

void oscEvent(OscMessage theOscMessage) {
  println("### received an osc message with addrpattern "+theOscMessage.addrPattern()+" and typetag "+theOscMessage.typetag());
  theOscMessage.print();
}

/* 
 //cursor mouse assistants
void cible() {
  stroke(#FFFFFF);
  strokeWeight(1);
  line(mouseX-5, mouseY-5, mouseX-5, mouseY+5);
}
*/
