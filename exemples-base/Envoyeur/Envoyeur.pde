import oscP5.*;
import netP5.*;

OscP5 oscP5;
NetAddress myRemoteLocation;


void setup() {
  size(400,400);
  /* start oscP5, listening for incoming messages at port 12000 */
  oscP5 = new OscP5(this,32000);
  
  /* myRemoteLocation is a NetAddress. a NetAddress takes 2 parameters,
   * an ip address and a port number. myRemoteLocation is used as parameter in
   * oscP5.send() when sending osc packets to another computer, device, 
   * application. usage see below. for testing purposes the listening port
   * and the port of the remote location address are the same, hence you will
   * send messages back to this sketch.
   */
  myRemoteLocation = new NetAddress("10.0.1.176",32000);

}

void draw() {
  background(0);
  cible();
  bonjour();
}

void bonjour () {
  /* in the following different ways of creating osc messages are shown by example */
  OscMessage myMessage = new OscMessage("/test");
  myMessage.add(mouseX); /* add an int to the osc message */
  myMessage.add(mouseY); /* add a float to the osc message */
  /* send the message */
  oscP5.send(myMessage, myRemoteLocation); 
}

void oscEvent(OscMessage theOscMessage) {
  println("### received an osc message with addrpattern "+theOscMessage.addrPattern()+" and typetag "+theOscMessage.typetag());
  theOscMessage.print();
}

 

/* */


void cible(){
  noFill();
  stroke(#E80909);
  strokeWeight(2);
  line(mouseX, mouseY-20, mouseX, mouseY+20);
  line(mouseX-20, mouseY, mouseX+20, mouseY);
  ellipse(mouseX,mouseY,30,30);
}
