
/**
 * oscP5broadcastClient by andreas schlegel
 * an osc broadcast client.
 * an example for broadcast server is located in the oscP5broadcaster exmaple.
 * oscP5 website at http://www.sojamo.de/oscP5
 */

import oscP5.*;
import netP5.*;


OscP5 oscP5;

/* a NetAddress contains the ip address and port number of a remote location in the network. */
NetAddress myBroadcastLocation; 

void setup() {
  size(400,400);
  frameRate(25);
  
  /* create a new instance of oscP5. 
   * 12000 is the port number you are listening for incoming osc messages.
   */
  oscP5 = new OscP5(this,32000);
  
  /* create a new NetAddress. a NetAddress is used when sending osc messages
   * with the oscP5.send method.
   */
  
  /* the address of the osc broadcast server, du diffuseur/controlleur */
  myBroadcastLocation = new NetAddress("192.168.0.170",32000);
}

void draw() {
  background(#999999);
  fill(#000000);
  rectMode(CORNERS);
  rect(200,200,250,firstValue*2);
  rectMode(CORNER);
  pushMatrix();
  translate(225,325);
  rotate(secondValue);
  rect(-25,-25,50,50);
  popMatrix();
  
}


void mousePressed() {
  /* create a new OscMessage with an address pattern, in this case /test. */
  OscMessage myOscMessage = new OscMessage("/test");
  /* add a value (an integer) to the OscMessage */
  myOscMessage.add(100);
  /* send the OscMessage to a remote location specified in myNetAddress */
  oscP5.send(myOscMessage, myBroadcastLocation);
}


void keyPressed() {
  OscMessage m;
  switch(key) {
    case('c'):
      /* connect to the broadcaster */
      m = new OscMessage("/server/connect",new Object[0]);
      oscP5.flush(m,myBroadcastLocation);  
      break;
    case('d'):
      /* disconnect from the broadcaster */
      m = new OscMessage("/server/disconnect",new Object[0]);
      oscP5.flush(m,myBroadcastLocation);  
      break;

  }  
}

float firstValue;
float secondValue;


/* incoming osc message are forwarded to the oscEvent method. */
void oscEvent(OscMessage theOscMessage) {
  if(theOscMessage.checkAddrPattern("/oscControl/slider3")==true) { 
    if(theOscMessage.checkTypetag("f")) { 
      firstValue = theOscMessage.get(0).floatValue(); 
      secondValue = map(firstValue,0,100,0,TWO_PI*100);
      firstValue = firstValue*100;
      println(" valeur: "+firstValue);
      return; 
    }  
  }
} 
