import netP5.*;
import oscP5.*;
import controlP5.*;

OscP5 oscP5;
OscP5 oscP5tcpServer;
OscP5 oscP5tcpClient;
NetAddress myBroadcastLocation; 

ControlP5 cp5;
Textarea myTextarea;

String resultat = "";

void setup() {
  size(500, 500);
  frameRate(25);

  cp5 = new ControlP5(this);
  
  oscP5tcpClient = new OscP5(this, "127.0.0.1", 32000, OscP5.TCP);
  
  oscP5 = new OscP5(this, 32000); // port d'écoute
  myBroadcastLocation = new NetAddress("127.0.0.1", 32000);


  myTextarea = cp5.addTextarea("txt")
    .setPosition(15, 15)
    .setSize(470, 300)
    .setLineHeight(14)
    .setColor(color(255))
    .setColorBackground(color(255, 100))
    .setColorForeground(color(255, 100));
  ;
}

void draw() {
  background(0);
  fill(255);
  textSize(25);
    myTextarea.setText(resultat);
}

void oscEvent(OscMessage theOscMessage) {
  println("### received an osc message with addrpattern "+theOscMessage.addrPattern()+" and typetag "+theOscMessage.typetag());
  theOscMessage.print();
  resultat = resultat + " " + theOscMessage.get(0).stringValue();
}
