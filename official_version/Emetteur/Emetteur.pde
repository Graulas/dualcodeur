import controlP5.*;
import java.util.*;

import netP5.*;
import oscP5.*;
import controlP5.*;


OscP5 oscP5;
ControlP5 cp5;
Textarea myTextarea;


NetAddress myRemoteLocation;


String myIp = "127.0.0.1";
String hisIp = "127.0.0.1";

int MHP = 12000; //port d'écoute
int HHP = 32000; // port d'écoute

int MBP = 32000; // Broadcaste, port d'envoi 
int HBP;// port d'envoi

String input = "";

void setup() {
  size(500, 500);
  
  PFont font = createFont("arial",25);

  oscP5 = new OscP5(this, MHP);
  cp5 = new ControlP5(this);

  myRemoteLocation = new NetAddress(hisIp, HHP);
  
  cp5.addTextfield("input")
     .setPosition(0,460)
     .setSize(width-75,40)
     .setFocus(true)
     .setColor(color(255,0,0))
     .setFont(font)
     ;
   
  cp5.addTextfield("hisdIp")
     .setPosition(0,0)
     .setSize(width/4,40)
     .setFocus(true)
     .setColor(color(255,0,0))
     .setFont(font)
     ;
     
     cp5.addTextfield("MHP")
     .setPosition(width/4,0)
     .setSize(width/4,40)
     .setFocus(true)
     .setColor(color(255,0,0))
     .setFont(font)
     ;
     
     cp5.addTextfield("HHP")
     .setPosition(width/2,0)
     .setSize(width/4,40)
     .setFocus(true)
     .setColor(color(255,0,0))
     .setFont(font)
     ;
     
  cp5.addButton("envoyer")
     .setPosition(width-75, 460)
     .setSize(75, 40)
     .updateSize();
     ;
     
  myTextarea = cp5.addTextarea("txt")
    .setPosition(1, 80)
    .setSize(width-2, 380)
    .setLineHeight(14)
    .setColor(color(255))
    .setColorBackground(color(255, 100))
    .setColorForeground(color(255, 100));
  ;

}

void draw() {
  background(0);
  fill(#00FF00);
  textSize(25);
  //myTextarea.setText(input);
  
}
public void envoyer() {

  input = cp5.get(Textfield.class, "input").getText().toString();

  OscMessage myMessage = new OscMessage("message");
  
  myMessage.add(input);

  oscP5.send(myMessage, myRemoteLocation);
  cp5.get(Textfield.class, "input").clear();
  
}
